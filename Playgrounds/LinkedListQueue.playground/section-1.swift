import Foundation

class Queue<T:NSObject> {
    var count: Int = 0
    var head: Node<T> = Node<T>() // Beginning of queue
    var tail: Node<T> = Node<T>() // End of queue
    
    init() {
        
    }
    
    func isEmpty() -> Bool {
        return self.count == 0
    }
    
    func enqueue(value: T) {
        // Creates node out of value arguement
        var node = Node<T>(value: value)
        // If empty, node is both head and tail of queue
        if self.isEmpty() {
            self.head = node
            self.tail = node
        } else {
            node.next = self.head
            self.head.prev = node
            self.head = node
        }
        self.count++
    }
    
    func dequeue() -> T? { // Returns optional generic
        if self.isEmpty() {
            return nil
        } else if self.count == 1 {
            var temp: Node<T> = self.tail // temporary node = tail
            self.count--
            return temp.value
        } else {
            var temp: Node<T> = self.tail
            self.tail = self.tail.prev!
            self.count--
            return temp.value
        }
    }
}

class Node<T:NSObject> {
    var value: T? =  nil // Optional generic "value"
    var next: Node<T>? = nil // Optional Node instance
    var prev: Node<T>? = nil
    
    init() {
        
    }
    
    init(value: T) {
        self.value = value
    }
}
