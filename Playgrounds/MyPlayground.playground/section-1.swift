class NamedShape
{
    var numOfSides:Int = 0
    var name:String
    
    init(name:String)
    {
        self.name = name
    }
    
    func description() -> String
    {
            return "A \(name) with \(numOfSides) sides."
    }
}

class Square : NamedShape
{
    var sideLength:Double
    
    init(sideLength: Double, name:String)
    {
        self.sideLength = sideLength
        super.init(name: name)
        numOfSides = 4
    }
    
    func area() -> Double
    {
        return sideLength * sideLength
    }
    
    override func description() -> String
    {
        return "A square with sides of length \(sideLength)"
    }
}

class Circle : NamedShape
{
    var radius:Double
    let pi = 3.14
    
    init(radius:Double, name:String)
    {
        self.radius = radius
        super.init(name: name)
    }
    
    func area() -> Double
    {
        return pi * (radius * radius)
    }
    
    override func description() -> String
    {
        return "A circle with area \(area())"
    }
}

class EquilateralTriangle : NamedShape
{
    var sideLength:Double = 0.0
    
    init(sideLength:Double, name:String)
    {
        self.sideLength = sideLength
        super.init(name:name)
        numOfSides = 3
    }
    
    var perimeter:Double {
    get {
        return 3.0 * sideLength
    }
    set {
        sideLength = newValue / 3.0
    }
    }
    
    override func description() -> String
    {
        return "An equilateral triangle with side length \(sideLength)"
    }
}

var shape = NamedShape(name:"Triangle")
shape.numOfSides = 3
var shapeDescription = shape.description()

let test = Square(sideLength: 15, name: "Square")
test.area()
test.description()

let test2 = Circle(radius: 3.5, name: "Circle")
test2.area()
test2.description()

var triangle = EquilateralTriangle(sideLength: 3.1, name: "Triangle")
triangle.perimeter
triangle.perimeter = 9.9
triangle.sideLength


class Counter
{
    var count:Int = 0
    
    func incrementBy(amount:Int, numberOftimes times:Int)
    {
        count += amount * times
    }
}

var counter = Counter()
counter.incrementBy(5, numberOftimes: 5)































