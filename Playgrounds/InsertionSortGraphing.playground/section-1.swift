import Cocoa
import XCPlayground

var data = [47, 21, 71, 97, 97, 84, 42, 48, 69, 57, 99, 55, 88, 90, 15, 80, 85, 54, 75, 61, 38]

func visualize<T>(data: [T], identifier: String) {
    for x in data {
        XCPCaptureValue(identifier, x)
    }
}
visualize(data, "Start")

func exchange<T>(data: [T], i: Int, j: Int) {
    // Swaps two values
    let temp =  data[i]
    data[i] = data[j]
    data[j] = temp
}

func swapLeft<T: Comparable>(data: [T], index: Int) {
    for i in reverse(1...index) {
        if data[i] < data[i - 1] {
            exchange(data, i, i-1)
        } else {
            break
        }
    }
    visualize(data, "Iteration \(index)")
}

func isort<T: Comparable>(data: [T]) {
    for i in 1..<data.count {
        swapLeft(data, i)
    }
}

isort(data)
data
