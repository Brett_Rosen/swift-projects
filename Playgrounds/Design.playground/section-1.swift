// -- Linked Lists (a recursive data type)
// Made up of "nodes"
// Each node has:
//      - an item
//      - a reference to next node in list

class ListNode {
    var item: Int
    var next: ListNode?
    
    init(item: Int, next: ListNode?) {
        self.item = item
        self.next = next
    }
    convenience init(item: Int) {
        self.init(item: item, next: nil)
    }
    
    func insertAfter(#item: Int) {
        next = ListNode(item: item, next: next)
    }
    func nth(#position: Int) -> ListNode? {
        if position == 1 {
            return self
        } else if position < 1 || !next  {
            return nil
        } else {
            return next?.nth(position: position - 1)
        }
    }
    
}

class SListNode {
    var item: AnyObject
    var next: SListNode?
    
    init(item: AnyObject, next: SListNode?) {
        self.item = item
        self.next = next
    }
    convenience init(item: AnyObject) {
        self.init(item: item, next: nil)
    }
}
class DListNode {
    var item: AnyObject
    var next: DListNode
    var prev: DListNode
    
    init(item: AnyObject, next: DListNode, prev: DListNode) {
        self.item = item
        self.next = next
        self.prev = prev
    }
}
// -- Invariants of SList:
//      - "size" is always correct
//      - List is never circularly linked
class SList {
    var head: SListNode?
    var size: Int
    
    init() {
        head = nil
        size  = 0
    }
    
    func insertFront(item: AnyObject) {
        head = SListNode(item: item, next: head)
        size++
    }
    func deleteFront() {
        if head !== nil {
            head = head?.next
            size--
        }
    }
}
// -- Sentinel:
//      - A special node that doesn't represent a data item
//      - Represents head and tail of DList
// -- Invariants of DList:
//      - For any DList d, d.head != nil
//      - For any DListNode x, x.next != nil
//      - For any DListNode x, x.prev != nil
//      - For any DListNode, if x.next == y, then y.prev == x
//      - For any DListNode, if x.prev == y, then y.next == x
//      - A DList "size" variable is # of DListNodes NOT counting sentinel
// -- In an empty DList, Sentinel prev and next point to itself
class DList {
    var head: DListNode // Sentinel
    var size: Int = 0
    
    init(head: DListNode) {
        self.head = head
    }
}

class Date {
    var day: Int
    var month: Int
    
    init(day: Int, month: Int) {
        self.day = day
        self.month = month
    }
    
    func setMonth(m: Int) {
        month = m
    }
}

// -- Interface of a class:
//      - Set of protoypes for public methods
//      - Descriptions of those methods behaviors
// -- Abstract Data Type (ADT):
//      - A class that has a well-defined interface
//        but it's implementation details are hidden
//        from other classes
//      - Not all classes are ADTs, some classes are
//        just data storage units (no invariants)
// -- Invariant:
//      - A fact about a data structure that is always true
//      ex: A Date object always represents a valid date


// -- Recursion base cases for binary search:
//      - findMe = middle element: return it's index
//      - Subarray of length zero: return failure
// -- How fast does BinarySearch run?
//      - n ... n/2 ... n/4 ... n/8 ... 1?
//      - Takes Log2n recursive bsearch calls
class BinarySearch {
    let failure = -1
    
    func bsearch(i: [Int], left: Int, right: Int, findMe: Int) -> Int {
        if left > right { return failure }
        
        var mid = (left + right) / 2
        if findMe == i[mid] {
            return mid
        } else if findMe < i[mid] {
            return bsearch(i, left: left, right: mid - 1, findMe: findMe)
        } else {
            return bsearch(i, left: mid + 1, right: right, findMe: findMe)
        }
    }
}










