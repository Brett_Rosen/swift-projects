class Card {
    var name: String
    var attack: Int
    var defense: Int
    var taunt: Bool
    var cost: Int
    
    init(name: String, attack: Int, defense: Int, taunt: Bool, cost: Int) {
        self.name = name
        self.attack = attack
        self.defense = defense
        self.taunt = taunt
        self.cost = cost
    }
    
    func attackPlayer(playerHealth: Player, creaturesInPlay: [Card]) {
        for card in creaturesInPlay {
            if card.taunt {
                "You cannot attack the player!"
            } else {
                playerHealth.health -= self.attack
                if playerHealth.health <= 0 {
                    "Player has been killed!"
                }
            }
        }
    }
}

class Player {
    var health = 30
    var mana = 1
    
    func endTurn() {
        self.mana += 1
    }
    func playCard(creature: Card, var creaturesInPlay: [Card]) {
        if self.mana >= creature.cost {
            creaturesInPlay.append(creature)
            self.mana -= creature.cost
        } else {
            "You do not have enough mana to play that card!"
        }
    }
}
class Priest: Player {
    func healSelf() {
        if self.mana >= 2 {
            if self.health == 30 {
                "You are already full health!"
            } else if health == 29 {
                self.health += 1
                self.mana -= 2
            } else {
                self.health += 2
                self.mana -= 2
            }
        } else {
            "You do not have mana for that!"
        }
    }
}
class Warrior: Player {
    var armor = 0
    
    func armorUp() {
        if self.mana >= 2 {
            self.health += 2
            self.armor += 2
            self.mana -= 2
        } else {
            "You do not have mana for that!"
        }
    }
}


var priest = Priest()
var inPlay: [Card] = []

var boulderfistOgre = Card(name: "Boulderfist Ogre", attack: 6, defense: 7, taunt: false, cost: 6)


boulderfistOgre.attackPlayer(priest, creaturesInPlay: inPlay)
priest.healSelf()
priest.playCard(boulderfistOgre, creaturesInPlay: inPlay)
