// --------------- SWIFT & PLAYGROUNDS



// Playgrounds are lightweight file wrappers which contain:
    // 1. Swift Code
    // 2. Folder of Embedded Resources
    // 3. Timeline Items for Visualizing Results



// Playgrounds are good for:
// 1. Trying out new code/APIs
// 2. Developing algorithms
// 3. Learning



// Playgrounds are not sandboxed - commands such as system("cd ~;rm -rf") will execute
// Careful when using/loading other people's code






































// 1. ------------- Variables
// No need for ";" at the end of each line, optional
// No need for main function, etc, can just start coding
// Type Inference, Explicit Declaration, Variables vs Constants

var language = "Swift"
var yearOfRelease = 2014
var oneMillion = 45_390_000

var version: Double = 1.0



let myConstant = 10





































// 2. ------------- Array/Dictionary
// Adding Elements

var names = ["Alex" , "Bob", "Joe", "Tom"]
names += "Steve"
names.append("Anthony")
names

var numberOfLegs = ["ant": 6, "snake": 0, "cheetah": 4]
numberOfLegs["spider"] = 8
numberOfLegs
























// 3. ------------- Loops
// Need {} even for single line statements

for _ in 1...5 { // Inclusive ... Exclusive ..
    ""
}
for var i = 0; i < 5; i++ {
    ""
}

for name in names {
    println("Hello, \(name)!")
}
for (animalName, legCount) in numberOfLegs {
    println("\(animalName) has \(legCount) legs.")
}

var count = 0
while (count < 10) {
    count++
}

























// 4. ------------- Optionals
// Handle the absence of a value

// Optional either returns Int or nil(no value)
let possibleLegCount: Int? = numberOfLegs["aardvark"]

var foo: Int? = 41
foo! + 1 // If nil, will get runtime error


if let value = foo {
    value + 1
} else { }

// Optional binding - If possibleLegCount doesnt return nil, set it equal to legCount and print
if let legCount = possibleLegCount {
    println("An aardvark has \(legCount) legs")
} else {
    "Aardvark does not exist in dictionary"
}






















// 5. ------------- Switch

let legCount = 2
switch legCount {
case 0:
    println("It has no legs")
case 1...8:
    println("It has few legs")
default:
    println("It has many legs")
}






















// 6. ------------- Functions

func sayHello(#name:String) {
    println("Hello \(name)!")
}
sayHello(name: "Brett")

func buildGreeting(#name:String) -> String {
    return "Hello " + name
}
let greeting = buildGreeting(name: "Jim")

func refreshWebPage() -> (code:Int, message:String) {
    // ...try to refresh...
    return (200, "Success")
}
let webStatus = refreshWebPage()
println("Received \(webStatus.code): \(webStatus.message)")

























// 7. ------------- Closures
// Self-contained blocks of functionality that can be passed around

// task is a closure
func repeat(count: Int, task: () -> ()) {
    for i in 0..count {
        task()
    }
}
// Trailing closure
repeat(2) {
    println("Hello!")
}


func applyMultiplication(value: Int, multFunction: Int -> Int) -> Int {
    return multFunction(value)
}

applyMultiplication(2) {
    $0 * 3
}




















// 8. ------------- Classes
//  Classes are passed by value

class ClassName {
    
}

class Vehicle {
    // Stored Property
    var numberOfWheels = 0
    // Computed Property
    var description: String{
        // Read only, no set property
        return "\(numberOfWheels) wheels"
    }
}
let someVehicle = Vehicle()
someVehicle.description

class Bicycle: Vehicle {
    init() {
        super.init()
        numberOfWheels = 2
    }
}
let myBicycle = Bicycle()
myBicycle.description

class Car: Vehicle {
    var speed = 0.0
    init() {
        super.init()
        numberOfWheels = 4
    }
    // Override description from superclass
    override var description: String {
        return super.description + ", \(speed) mph"
    }
}
let myCar = Car()
myCar.speed = 25.0
myCar.description

// Property Obsertvers
class ParentsCar: Car {
    override var speed: Double {
        willSet {
            // newValue is available here
            if newValue > 65.0 {
                println("Careful now.")
            }
        }
    }
}
let myParentsCar = ParentsCar()
myParentsCar.speed = 67.0

class Counter {
    var count  = 0
    
    func incrementBy(amount: Int) {
        count += amount
    }
    
}
let myCounter = Counter()
myCounter.incrementBy(5)






// 8. ------------- Structures 
//  Structures cannot inherit from other structures
//  Structures are passed by value

struct Point {
    var x, y: Double
    
    mutating func moveToTheRigtBy(dx: Double) {
        x += dx
    }
}

struct Size {
    var width, height: Double
}

struct Rect {
    // Stored Properties
    var origin: Point
    var size: Size
    
    // Computed Property
    var area: Double {
        return size.width * size.height
    }
    
    func isBiggerThanRect(other: Rect) -> Bool {
        return self.area > other.area
    }
}

var point = Point(x: 0.0, y: 0.0)
var size = Size(width: 640.0, height: 500.0)
var size2 = Size(width: 300.0, height: 150.0)

var rect = Rect(origin: point, size: size)
var rect2 = Rect(origin: point, size: size2)

println("Is rect bigger than rect2: \(rect.isBiggerThanRect(rect2))")





// 9. ------------- Enumeration

enum Planet: Int {
    case Mercury = 1, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune
}
//toRaw gives back index of an enumerated var
let earthNumber = Planet.Earth.toRaw() // 3


enum ControlCharacter: Character {
    case Tab = "\t"
    case Linefeed = "\n"
    case CarriageReturn = "\r"
}

enum CompassPoint {
    case North, South, East, West
}
var direction = CompassPoint.West
// Compiler now knows direction is a CompassPoint, so we can skip the CompassPoint prefix
direction = .East
direction = .South

class Train {
    enum Status {
        case OnTime, Delayed(Int)
        init() {
            // Automatically OnTime when TrainStatus called
            self = OnTime
        }
        
        // Computed Property
        var description: String {
        switch self {
        case OnTime:
            return "on time"
        case Delayed(let minutes):
            return "Train delayed by \(minutes) minutes"
            }
        }
    }
    var status = Status()
}

let myTrain = Train()
myTrain.status = .Delayed(5)
myTrain.status.description





// 10. ------------- Extentions
// Added functionaility to Class, Structure, Enum

extension Size {
    mutating func increaseByFactor(factor: Double) {
        width *= factor
        height *= factor
    }
}

extension Int {
    // task is closure
    func repetitions(task: () -> ()) {
        for i in 0..self {
            task()
        }
    }
}

20.repetitions{
    println("Hello")
}





// 11. ------------- Generics
// Allows code that works with any type

struct Stack<T> {
    var elements = T[]() // Generic Array
    
    mutating func push(element: T) {
        elements.append(element)
    }
    mutating func pop() -> T {
        return elements.removeLast()
    }
}
var intStack = Stack<Int>()
intStack.push(50)
intStack.pop()

var stringStack = Stack<String>()
stringStack.push("Hello!")
stringStack.pop()























