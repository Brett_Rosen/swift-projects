import Cocoa

var oneCount = 0
var zeroCount = 0

var onePlayerZeroComputer  = Int(arc4random_uniform(2))

for _ in 1...10 {
    if onePlayerZeroComputer == 1 {
        oneCount++
    } else { zeroCount++ }
    onePlayerZeroComputer  = Int(arc4random_uniform(2))
}