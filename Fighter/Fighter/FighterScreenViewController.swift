//
//  FighterScreenViewController.swift
//  Fighter
//
//  Created by Brett Rosen on 12/15/15.
//  Copyright © 2015 Brett Rosen. All rights reserved.
//

import UIKit

class FighterScreenViewController: UIViewController {

    @IBOutlet weak var testLabel: UILabel!
    
    var titleString: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.testLabel.text = titleString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
