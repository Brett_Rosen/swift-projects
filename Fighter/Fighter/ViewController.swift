//
//  ViewController.swift
//  Fighter
//
//  Created by Brett Rosen on 12/15/15.
//  Copyright © 2015 Brett Rosen. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var fighterTableView: UITableView!
    
    var tableObjects: NSMutableArray! = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.tableObjects.addObject("empty")
        self.fighterTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table View
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableObjects.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.fighterTableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! MainMenuTableViewCell
        
        cell.textLabel?.text = self.tableObjects.objectAtIndex(indexPath.row) as? String
        
        cell.nameButton.tag = indexPath.row
        //cell.nameButton.addTarget(self, action: "logFighter:", forControlEvents: .TouchUpInside)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("showView", sender: self)
    }
    
    @IBAction func logFighter(sender: UIButton) {
        let titleString = self.tableObjects.objectAtIndex(sender.tag) as? String
        let firstActivityItem = "\(titleString)"
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [firstActivityItem], applicationActivities: nil)
        
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showView") {
            var upcoming: FighterScreenViewController = segue.destinationViewController as! FighterScreenViewController
            
            let indexPath = self.fighterTableView.indexPathForSelectedRow
            let titleString = self.tableObjects.objectAtIndex((indexPath?.row)!) as? String
            
            upcoming.titleString = titleString
            
            self.fighterTableView.deselectRowAtIndexPath(indexPath!, animated: true)
        }
    }

}

