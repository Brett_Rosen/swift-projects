// add 5 to each ride
// add to array
// sort array

var sortedArray = [10, 13, 15, 17, 20]
var numberOfRides = 0

func findBestSolution(array: [Int]) -> Int {
    
    var time = 0

    for var i = 0; i < array.count; i++ {
        
        if (time + array[i] > 60) {
            return time
        }
        else {
            time = time + array[i]
            numberOfRides++
        }
        
    }
    
    return time
}

