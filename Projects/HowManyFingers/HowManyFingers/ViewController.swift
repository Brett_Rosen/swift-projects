//
//  ViewController.swift
//  HowManyFingers
//
//  Created by Brett Rosen on 5/12/15.
//  Copyright (c) 2015 Brett Rosen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var gameMessage: UILabel!
    
    var randomNumber: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        resetGame()
    }
    
    @IBAction func guessPressed(sender: AnyObject) {
        
        if let guessedNumber = textField.text.toInt() {
            
            if guessedNumber == randomNumber {
                gameMessage.text = "You win!"
            } else {
                gameMessage.text = "Nope!"
            }
        }
        
    }
    
    @IBAction func replayPressed(sender: AnyObject) {
        resetGame()
    }
    
    func resetGame() {
        randomNumber = Int(arc4random_uniform(6))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

