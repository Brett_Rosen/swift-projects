//
//  ViewController.swift
//  DogYears
//
//  Created by Brett Rosen on 5/8/15.
//  Copyright (c) 2015 Brett Rosen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func findAgePressed(sender: AnyObject) {
        println("User entered: \(textField.text)")
        
        var resultAge: Int
        
        if let enteredAge = textField.text.toInt() {
            resultAge = enteredAge * 7
            resultLabel.text = "Your dog is \(resultAge) years old"
        } else {
            resultLabel.text = "You did not enter a number"
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

