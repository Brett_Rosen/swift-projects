//
//  ViewController.swift
//  DoUHave
//
//  Created by Brett Rosen on 6/1/15.
//  Copyright (c) 2015 Brett Rosen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var resultLabel: UILabel!
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func switchChanged(sender: UISwitch) {
        
        if sender.on {
            count++
        } else {
            count--
        }
        
    }
    
    @IBAction func finishPressed(sender: AnyObject) {
        if count == 0 {
            resultLabel.text = "You're lucky you don't have autism."
        } else if count > 1 && count < 3{
            resultLabel.text = "You're a little autistic."
        } else {
            resultLabel.text = "You're full blown autistic."
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

