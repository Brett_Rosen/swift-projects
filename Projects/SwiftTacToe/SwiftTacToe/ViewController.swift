import UIKit

class ViewController: UIViewController {
    // TicTac Image Outlets
    @IBOutlet var ticTacImg1: UIImageView? = nil
    @IBOutlet var ticTacImg2: UIImageView? = nil
    @IBOutlet var ticTacImg3: UIImageView? = nil
    @IBOutlet var ticTacImg4: UIImageView? = nil
    @IBOutlet var ticTacImg5: UIImageView? = nil
    @IBOutlet var ticTacImg6: UIImageView? = nil
    @IBOutlet var ticTacImg7: UIImageView? = nil
    @IBOutlet var ticTacImg8: UIImageView? = nil
    @IBOutlet var ticTacImg9: UIImageView? = nil
    // TicTac Button Outlets
    @IBOutlet var ticTacBttn1: UIButton? = nil
    @IBOutlet var ticTacBttn2: UIButton? = nil
    @IBOutlet var ticTacBttn3: UIButton? = nil
    @IBOutlet var ticTacBttn4: UIButton? = nil
    @IBOutlet var ticTacBttn5: UIButton? = nil
    @IBOutlet var ticTacBttn6: UIButton? = nil
    @IBOutlet var ticTacBttn7: UIButton? = nil
    @IBOutlet var ticTacBttn8: UIButton? = nil
    @IBOutlet var ticTacBttn9: UIButton? = nil
    // TicTac Reset/Message Outlets
    @IBOutlet var resetButton: UIButton? = nil
    @IBOutlet var userMessage: UILabel = nil
    
    var plays = Dictionary<Int, Int>() // <Button Tag, Mark>
    var done = false

    
    @IBAction func UIButtonClicked(sender: UIButton) {
        userMessage.hidden = true
        // if there's no mark at the button tag && game not done
        if !plays[sender.tag] && !done {
            setImageForSpot(spot: sender.tag, tag: 1)
            checkForWin()
            checkForTie()
        }
        aiTurn()

    }
    
    func setImageForSpot(#spot: Int, tag: Int) {
        var mark = tag == 1 ? "x" : "o"
        plays[spot] = tag
        
        switch spot {
        case 1:
            ticTacImg1.image = UIImage(named: mark)
        case 2:
            ticTacImg2.image = UIImage(named: mark)
        case 3:
            ticTacImg3.image = UIImage(named: mark)
        case 4:
            ticTacImg4.image = UIImage(named: mark)
        case 5:
            ticTacImg5.image = UIImage(named: mark)
        case 6:
            ticTacImg6.image = UIImage(named: mark)
        case 7:
            ticTacImg7.image = UIImage(named: mark)
        case 8:
            ticTacImg8.image = UIImage(named: mark)
        case 9:
            ticTacImg9.image = UIImage(named: mark)
        default:
            println("Error - setImageForSpot()")
        }
    }
    
    func checkForWin() {
        var whoWon = ["Computer": 0, "You": 1]
        for (key, value) in whoWon {
            
            // Check win conditions
            if (plays[7] == value && plays[8] == value && plays[9] == value) ||  // Across Bottom
                (plays[4] == value && plays[5] == value && plays[6] == value) || // Across Middle
                (plays[1] == value && plays[2] == value && plays[3] == value) || // Across Top
                (plays[1] == value && plays[4] == value && plays[7] == value) || // Down Left
                (plays[2] == value && plays[5] == value && plays[8] == value) || // Down Middle
                (plays[3] == value && plays[6] == value && plays[9] == value) || // Down Right
                (plays[1] == value && plays[5] == value && plays[9] == value) || // Diagonal TopLeft - BottomRight
                (plays[3] == value && plays[5] == value && plays[7] == value) {  // Diagonal TopRight - BottomLeft
                    userMessage.hidden = false // Show winning message
                    userMessage.text = "\(key) won!"
                    resetButton.hidden = false
                    done = true
            }
        }
    }
    
    func checkForTie() {
        var numberOfTilesFilled = 0
        for i in 1...9 {
            if plays[i] {
                numberOfTilesFilled++
            }
        }
        if numberOfTilesFilled == 9 {
            userMessage.hidden = false // Show tie message
            userMessage.text = "Tie!"
            resetButton.hidden = false
            done = true
        }
    }
    
    func aiTurn() {
        var possibleArrangements = ["112", "211", "121"]
        
        // Game has been won, exit function
        if done {
            return
        }
        // First, check if middle is open, if it is, mark
        if !plays[5] {
            setImageForSpot(spot: 5, tag: 0)
            return
        }
        // Check Top Row
        if check(patterns: possibleArrangements, slot1: 1, slot2: 2, slot3: 3, identifier: "Top Row") == 0 { }
        else { setImageForSpot(spot: check(patterns: possibleArrangements, slot1: 1, slot2: 2, slot3: 3, identifier: "Top Row"), tag: 0)
            return
        }
        // Check Middle Row
        if check(patterns: possibleArrangements, slot1: 4, slot2: 5, slot3: 6, identifier: "Middle Row") == 0 { }
        else { setImageForSpot(spot: check(patterns: possibleArrangements, slot1: 4, slot2: 5, slot3: 6, identifier: "Middle Row"), tag: 0)
            return
        }
        // Check Bottom Row
        if check(patterns: possibleArrangements, slot1: 7, slot2: 8, slot3: 9, identifier: "Bottom Row") == 0 { }
        else { setImageForSpot(spot: check(patterns: possibleArrangements, slot1: 7, slot2: 8, slot3: 9, identifier: "Bottom Row"), tag: 0)
            return
        }
        // Check Left Col
        if check(patterns: possibleArrangements, slot1: 1, slot2: 4, slot3: 7, identifier: "Left Col") == 0 { }
        else { setImageForSpot(spot: check(patterns: possibleArrangements, slot1: 1, slot2: 4, slot3: 7, identifier: "Left Col"), tag: 0)
            return
        }
        // Check Middle Col
        if check(patterns: possibleArrangements, slot1: 2, slot2: 5, slot3: 8, identifier: "Middle Col") == 0 { }
        else { setImageForSpot(spot: check(patterns: possibleArrangements, slot1: 2, slot2: 5, slot3: 8, identifier: "Middle Col"), tag: 0)
            return
        }
        // Check Right Col
        if check(patterns: possibleArrangements, slot1: 3, slot2: 6, slot3: 9, identifier: "Right Col") == 0 { }
        else { setImageForSpot(spot: check(patterns: possibleArrangements, slot1: 3, slot2: 6, slot3: 9, identifier: "Right Col"), tag: 0)
            return
        }
        // Check Diag Left
        if check(patterns: possibleArrangements, slot1: 1, slot2: 5, slot3: 9, identifier: "Diag Left") == 0 { }
        else { setImageForSpot(spot: check(patterns: possibleArrangements, slot1: 1, slot2: 5, slot3: 9, identifier: "Diag Left"), tag: 0)
            return
        }
        // Check Diag Right
        if check(patterns: possibleArrangements, slot1: 3, slot2: 5, slot3: 7, identifier: "Diag Right") == 0 { }
        else { setImageForSpot(spot: check(patterns: possibleArrangements, slot1: 3, slot2: 5, slot3: 7, identifier: "Diag Right"), tag: 0)
            return
        }
        // If computer is open to not play defensively
        var randomNum = Int(arc4random_uniform(8) + 1)
        while checkIfEmpty(randomNum) == false {
            randomNum = Int(arc4random_uniform(8) + 1)
            println("Computer Choosing Random Number: \(random)")
        }
        setImageForSpot(spot: randomNum, tag: 0)
        /*for i in 1...9 {
            if !plays[i] {
                setImageForSpot(spot: i, tag: 0)
                break
            }
        }*/
    }
    
    func checkIfEmpty(num: Int) -> Bool {
        if !plays[num] {
            return true
        }
        return false
    }
    
    func check(#patterns: [String], slot1: Int, slot2: Int, slot3: Int, identifier: String) -> Int {
        var builtString = ""
        var count = 1
        if plays[slot1] { builtString += "\(plays[slot1])" } else { builtString += "2" }
        if plays[slot2] { builtString += "\(plays[slot2])" } else { builtString += "2" }
        if plays[slot3] { builtString += "\(plays[slot3])" } else { builtString += "2" }
        
        for pattern in patterns {
            if builtString == pattern {
                for slot in pattern {
                    if slot == "2" {
                        switch identifier {
                        case "Top Row":
                                return count
                        case "Middle Row":
                            if count == 1 { return 4 } else
                            if count == 2 { return 5 } else
                            if count == 3 { return 6 }
                        case "Bottom Row":
                            if count == 1 { return 7 } else
                            if count == 2 { return 8 } else
                            if count == 3 { return 9 }
                        case "Left Col":
                            if count == 1 { return 1 } else
                            if count == 2 { return 4 } else
                            if count == 3 { return 7 }
                        case "Middle Col":
                            if count == 1 { return 2 } else
                            if count == 2 { return 5 } else
                            if count == 3 { return 8 }
                        case "Right Col":
                            if count == 1 { return 3 } else
                            if count == 2 { return 6 } else
                            if count == 3 { return 9 }
                        case "Diag Left":
                            if count == 1 { return 1 } else
                            if count == 2 { return 5 } else
                            if count == 3 { return 9 }
                        case "Diag Right":
                            if count == 1 { return 3 } else
                            if count == 2 { return 5 } else
                            if count == 3 { return 7 }
                        default:
                            return 0
                        }
                    }
                    count++
                }
            }
        }
        return 0
    }
    
 
    @IBAction func resetClicked(sender: UIButton) {
        done = false
        resetButton.hidden = true
        userMessage.hidden = true
        
        plays = [:] // Erase dictionary
        ticTacImg1.image = nil
        ticTacImg2.image = nil
        ticTacImg3.image = nil
        ticTacImg4.image = nil
        ticTacImg5.image = nil
        ticTacImg6.image = nil
        ticTacImg7.image = nil
        ticTacImg8.image = nil
        ticTacImg9.image = nil
    }
                         
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

