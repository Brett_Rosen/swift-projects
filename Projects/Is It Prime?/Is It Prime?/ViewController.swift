//
//  ViewController.swift
//  Is It Prime?
//
//  Created by Brett Rosen on 5/20/15.
//  Copyright (c) 2015 Brett Rosen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var isItPrimeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func primeButtonPressed(sender: UIButton) {
        if let number = numberTextField.text.toInt() {
            var isPrime = true
            
            if number == 1 {
                isPrime = false
                
            }
            
            if number != 2 && number != 1{
                loop: for var i = 2; i < number; i++ {
                    
                    if number % i == 0 {
                        isPrime = false
                        break loop
                    }
                }
            }
            
            if isPrime == true {
                isItPrimeLabel.text = "\(numberTextField.text) is a prime number!"
            } else {
                isItPrimeLabel.text = "\(numberTextField.text) isn't a prime number!"
            }
            
            
        } else {
            isItPrimeLabel.text = "Enter a whole number!"
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

