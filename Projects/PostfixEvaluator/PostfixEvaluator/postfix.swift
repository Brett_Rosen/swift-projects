//
//  postfix.swift
//  PostfixEvaluator
//
//  Created by Brett Rosen on 6/27/14.
//  Copyright (c) 2014 Brett Rosen. All rights reserved.
//

import Foundation

class postfix
{
    struct IntStack {
        var items = Int[]()
        mutating func push(item: Int) {
            items.append(item)
        }
        mutating func pop() -> Int {
            return items.removeLast()
        }
        func returnFinalValue() -> Int {
            return items[0]
        }
        mutating func clearStack() {
            items = Int[]()
        }
    }
    
    var stack = IntStack()
    
    func determineToken(token:Character)
    {
        if token == "+" || token == "-" || token == "*" || token == "%" {
            var rightHand = stack.pop()
            var leftHand = stack.pop()
            var result:Int = evaluateSubExpression(leftHand, rightHand: rightHand, token: token)
            stack.push(result)
            print("Convert Failed]")
        } else if token == " " {
            print("Blank Token]")
        } else {
            var tokenAsInt:Int = String(token).toInt()!
            print("Convert Success]")
            stack.push(tokenAsInt)
        }
    }
    
    func evaluateSubExpression(leftHand:Int, rightHand:Int, token:Character) -> Int
    {
        var result:Int
        
        switch token
        {
        case "+":
            result = leftHand + rightHand
            return result
        case "-":
            result = leftHand - rightHand
            return result
        case "*":
            result = leftHand * rightHand
            return result
        case "/":
            result = leftHand / rightHand
            return result
        default:
            println("evaluateSubExpression - token invalid!")
            return 0
        }
    }
}