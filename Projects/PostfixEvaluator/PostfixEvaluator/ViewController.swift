//
//  ViewController.swift
//  PostfixEvaluator
//
//  Created by Brett Rosen on 6/27/14.
//  Copyright (c) 2014 Brett Rosen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var expressionTextField: UITextField
    @IBOutlet var calculateButton: UIButton
    @IBOutlet var expressionLabel: UILabel
    @IBOutlet var evaluationLabel: UILabel
    
    var post = postfix()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func calculateTap(sender: AnyObject) {
        post.stack.clearStack()
        var sExpressionTextfield:String =  expressionTextField.text
    
        for token in sExpressionTextfield {
            print("[token: " + token + " ")
            post.determineToken(token)
        }
        println("")
        println("Evaluation: \(post.stack.returnFinalValue())")
        expressionLabel.text = String(expressionTextField.text)
        evaluationLabel.text = "\(post.stack.returnFinalValue())"
    }

    // Resign keyboard on view tap
    @IBAction func viewTap(sender: AnyObject) {
        expressionTextField.resignFirstResponder()
    }
}

