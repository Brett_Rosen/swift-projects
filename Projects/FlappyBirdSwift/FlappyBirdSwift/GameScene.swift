import SpriteKit

class GameScene: SKScene {
    
    var bird = SKSpriteNode()
    var pipeUpTexture = SKTexture()
    var pipeDownTexture = SKTexture()
    var pipesMoveAndRemove = SKAction()
    
    let pipeGap = 150.0
    
    // Set up your scene here
    override func didMoveToView(view: SKView) {
        
        // Set gravity direction, downwards Y
        self.physicsWorld.gravity = CGVectorMake(0.0, -5.0)
        
        // ====================| BIRD | ==================== //
        
        // SKTexture added to SKSpriteNode
        var birdTexture = SKTexture(imageNamed: "fly1")
        birdTexture.filteringMode = SKTextureFilteringMode.Nearest
        // Set the bird's texture to "fly1"
        bird = SKSpriteNode(texture: birdTexture)
        bird.setScale(0.8)
        // Set bird's position to left of screen and a little above halfway point
        bird.position = CGPointMake(self.frame.size.width * 0.35, self.frame.size.height * 0.6)
        // Takes radius of bird and creates a circle radius body around it
        bird.physicsBody = SKPhysicsBody(circleOfRadius: bird.size.height / 2)
        bird.physicsBody.dynamic = true
        bird.physicsBody.allowsRotation = false
        
        // ====================| GROUND | ==================== //
        
        var groundTexture = SKTexture(imageNamed: "ground")
        var groundSprite = SKSpriteNode(texture: groundTexture)
        var ground = SKNode()
        groundSprite.setScale(2.0)
        groundSprite.position = CGPointMake(self.size.width / 2, groundSprite.size.height / 2)
        ground.position = CGPointMake(0, groundTexture.size().height)
        ground.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(self.frame.size.width, groundTexture.size().height * 2.0))
        ground.physicsBody.dynamic = false
        
        // ====================| PIPES | ==================== //
        
        pipeUpTexture = SKTexture(imageNamed: "pipe_bottom")
        pipeDownTexture = SKTexture(imageNamed: "pipe_top")
        
        // Grabs size of frame and width of frame, adds 2, then gets pipeTexture width
        let distanceToMove = CGFloat(self.frame.size.width + 2.0 * pipeUpTexture.size().width)
        let movePipes = SKAction.moveByX(-distanceToMove, y: 0.0, duration: NSTimeInterval(0.01 * distanceToMove))
        let removePipes = SKAction.removeFromParent()
        
        pipesMoveAndRemove = SKAction.sequence([movePipes, removePipes])
        
        func spawnPipes() {
            let pipePair = SKNode()
            pipePair.position = CGPointMake(self.frame.size.width + pipeUpTexture.size().width * 2, 0.0)
            // Set the position of the pipe to be behind the ground and bird
            pipePair.zPosition = -10
            
            let height = UInt32(self.frame.size.height / 4)
            let y = arc4random() % height + height
            
            let pipeDown = SKSpriteNode(texture: pipeDownTexture)
            pipeDown.setScale(2.0)
            pipeDown.position = CGPointMake(0.0, CGFloat(y) + pipeDown.size.height + CGFloat(pipeGap))
            
            pipeDown.physicsBody = SKPhysicsBody(rectangleOfSize: pipeDown.size)
            pipeDown.physicsBody.dynamic = false
            pipePair.addChild(pipeDown)
            
            let pipeUp = SKSpriteNode(texture: pipeUpTexture)
            pipeUp.setScale(2.0)
            pipeDown.position = CGPointMake(0.0, CGFloat(y))
            
            pipeUp.physicsBody = SKPhysicsBody(rectangleOfSize: pipeUp.size)
            pipeUp.physicsBody.dynamic = false
            pipePair.addChild(pipeUp)
            
            pipePair.runAction(pipesMoveAndRemove)
            self.addChild(pipePair)
        }
        
        let delay = SKAction.waitForDuration(NSTimeInterval(2))
        let spawn = SKAction.runBlock({() in spawnPipes()})
        let spawnThenDelay = SKAction.sequence([spawn, delay])
        let spawnThenDelayForever = SKAction.repeatActionForever(spawnThenDelay)
        self.runAction(spawnThenDelayForever)
        
        
        
        // ====================| SCENE | ==================== //
    
        self.addChild(bird)
        self.addChild(groundSprite)
        self.addChild(ground)
    }
    
    // Called when a touch begins
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        for touch: AnyObject in touches {
            // location is inside scene
            let location = touch.locationInNode(self)
            
            bird.physicsBody.velocity = CGVectorMake(0, 0)
            bird.physicsBody.applyImpulse(CGVectorMake(0, 25))
        }
    }
   
    // Called before each frame is rendered
    override func update(currentTime: CFTimeInterval) {
        
    }
}
