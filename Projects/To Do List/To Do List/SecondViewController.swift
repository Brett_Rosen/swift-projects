//
//  SecondViewController.swift
//  To Do List
//
//  Created by Brett Rosen on 5/29/15.
//  Copyright (c) 2015 Brett Rosen. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var itemTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.itemTextField.delegate = self
    }

    @IBAction func addItemPressed(sender: AnyObject) {
        
        toDoList.append(itemTextField.text)
        itemTextField.text = ""
        NSUserDefaults.standardUserDefaults().setObject(toDoList, forKey: "toDoList")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        itemTextField.resignFirstResponder()
        return true
    }
    
}

