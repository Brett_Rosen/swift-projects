//
//  ViewController.swift
//  Permanent Storage
//
//  Created by Brett Rosen on 5/29/15.
//  Copyright (c) 2015 Brett Rosen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NSUserDefaults.standardUserDefaults().setObject("Brett", forKey: "name")
        var name = NSUserDefaults.standardUserDefaults().objectForKey("name")! as String
        println(name)
        
        var arr = [1,2,3]
        NSUserDefaults.standardUserDefaults().setObject(arr, forKey: "array")
        var recalledArray = NSUserDefaults.standardUserDefaults().objectForKey("array")! as NSArray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

