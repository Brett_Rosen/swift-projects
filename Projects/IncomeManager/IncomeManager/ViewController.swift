//
//  ViewController.swift
//  IncomeManager
//
//  Created by Brett Rosen on 8/10/14.
//  Copyright (c) 2014 Brejuro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
                            
    @IBOutlet weak var incomeTextField: UITextField!
    @IBOutlet weak var incomeLabel: UILabel!
    @IBOutlet weak var startEarningButton: UIButton!
    @IBOutlet weak var earnedLabel: UILabel!
    
    var incomeHasBeenEntered = false
    
    var timerVal = 0
    var timer: NSTimer!
    
    var incomePerSecond = 0.0
    var cumulativeEarned = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func startTimer() {
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateTimer:"), userInfo: nil, repeats: true)
    }
    
    func updateTimer(dt: NSTimer) {
        timerVal++
        cumulativeEarned += incomePerSecond
        earnedLabel.text = NSString(format:"%.3f $", cumulativeEarned)
    }



    @IBAction func startEarningButtonClick(sender: AnyObject) {
        
        if incomeHasBeenEntered {
            incomeLabel.text = "0 $/hr"
            earnedLabel.text = "0 $"
            
            incomePerSecond = 0.0
            cumulativeEarned = 0.0
        }
        
        incomeLabel.text = incomeTextField.text + " $/hr"
        var incomeTextFieldString = NSString(string: incomeTextField.text)
        incomePerSecond = (incomeTextFieldString.doubleValue / 60) / 60
        earnedLabel.text = NSString(format:"%.3f $", cumulativeEarned)
        
        if !incomeHasBeenEntered {
            startTimer()
            incomeHasBeenEntered = true
        }
    }
}

