//
//  ViewController.swift
//  Navigation Bars
//
//  Created by Brett Rosen on 5/21/15.
//  Copyright (c) 2015 Brett Rosen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var timeLabel: UILabel!
    
    var timer = NSTimer()
    var count = 0
    
    func updateTime() {
        count++
        timeLabel.text = String(count)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
    @IBAction func startPressed(sender: AnyObject) {
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("updateTime"), userInfo: nil, repeats: true)
    }
    @IBAction func stopPressed(sender: AnyObject) {
        timer.invalidate()
        count = 0
        timeLabel.text = "0"
    }
    @IBAction func pausePressed(sender: AnyObject) {
        timer.invalidate()
    }
    @IBAction func resetPressed(sender: AnyObject) {
        count = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

