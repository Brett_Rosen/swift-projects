#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int readGrades(double (*grades)[]);
void frequency(double grades[], int numOfGrades);
int maximum(double grades[], int numOfGrades);
int minimum(double grades[], int numOfGrades);
double mean(double grades[], int numOfGrades);
double standardDeviation(double grades[], int numOfGrades);


int main() {
    
    double grades[100];
    int i;
    
    // Initialize grades values to 0
    for (i = 0; i < sizeof(grades)/sizeof(double); i++) {
        grades[i] = 0;
    }
    
    int numOfGradesRead = readGrades(&grades);
    
    int minIndex = minimum(grades, numOfGradesRead);
    printf("The data has been adjusted by removing the minimum %.2f\n", grades[minIndex]);
    
    int maxIndex = maximum(grades, numOfGradesRead);
    printf("The data has been adjusted by removing the maximum %.2f\n", grades[maxIndex]);
    
    int newNumOfGrades = deleteElement(&grades, numOfGradesRead, maxIndex);
        newNumOfGrades = deleteElement(&grades, newNumOfGrades, minIndex);
    
    double meanValue = mean(grades, newNumOfGrades);
    printf("The adjusted mean is %.2f\n", meanValue);
    
    double stddevValue = standardDeviation(grades, newNumOfGrades);
    printf("The adjusted standard deviation is %.2f\n\n", stddevValue);
    
    printf("Here is a histogram of the adjusted data: \n\n");
    frequency(grades, newNumOfGrades);
    
    return 0;
}

/*
 * readGrades(double (*grades)[])
 * INPUT:  reference to a double array of grades
 * OUTPUT: number of grades read
 */
int readGrades(double (*grades)[]) {
    
    int numOfGradesRead = 0,
    count = 0,
    numRead;
    
    char buf[1000];
    FILE *file = fopen("input.txt", "r");
    
    if (file == NULL) {
        perror("Can't open file");
    } else {
        while (fgets(buf, sizeof(buf), file)) {
            
            // Convert buf to integer
            numRead = atoi(buf);
            
            // Add number read to grades[]
            if (numRead != -999) {
                (*grades)[count] = numRead;
                numOfGradesRead++;
                count++;
            }
        }
    }
    
    fclose(file);
    
    return numOfGradesRead;
}

/*
 * frequency(double grades[], int numOfGrades)
 * INPUT:  double array of grades, amount of grades
 * OUTPUT: frequency histogram
 */
void frequency(double grades[], int numOfGrades) {
    
    int range = 0,
    numbOfRanges = 21,
    i, j;
    
    for (i = 0; i < numbOfRanges; i++) {
        if (i < 20) {
            printf("%d-%d| ", range, range + 4);
        } else {
            printf("100| ");
        }
        
        for (j = 0; j < numOfGrades; j++) {
            if (grades[j] >= range  && grades[j] <= range + 4) {
                printf("*");
            }
            if (i == 21) {
                if (grades[j] == 100) {
                    printf("*");
                }
            }
        }
        
        range += 5;
        printf("\n");
    }
    
}

/*
 * maximum(double grades[], int numOfGrades)
 * INPUT:  double array of grades, amount of grades
 * OUTPUT: index of the maximum grade
 */
int maximum(double grades[], int numOfGrades) {
    int indexOfMaxValue = 0,
    max = grades[0],
    i;
    
    for (i = 0; i < numOfGrades; i++) {
        if (grades[i] > max) {
            max = grades[i];
            indexOfMaxValue = i;
        }
    }
    
    return indexOfMaxValue;
}

/*
 * minimum(double grades[], int numOfGrades)
 * INPUT:  double array of grades, amount of grades
 * OUTPUT: index of the minimum grade
 */
int minimum(double grades[], int numOfGrades) {
    int indexOfMinValue = 0,
    min = grades[0],
    i;
    
    for (i = 0; i < numOfGrades; i++) {
        if (grades[i] < min) {
            min = grades[i];
            indexOfMinValue = i;
        }
    }
    
    return indexOfMinValue;
}

/*
 * deleteElement(double (*grades)[], int numOfGrades, int index)
 * INPUT:  pointer to double array of grades, amount of grades, index to be deleted
 * OUTPUT: updated amount of grades
 */
int deleteElement(double (*grades)[], int numOfGrades, int index) {
    
    int i;
    
    for (i = index; i < numOfGrades; i++) {
        (*grades)[i] = (*grades)[i+1];
    }
    
    return numOfGrades - 1;
}

/*
 * mean(double grades[], int numOfGrades)
 * INPUT:  double array of grades & amount of grades
 * OUTPUT: mean value of grades
 */
double mean(double grades[], int numOfGrades) {
    
    double sum = 0;
    double mean;
    int i;
    
    for (i = 0; i < numOfGrades; i++) {
        sum = sum + grades[i];
    }
    
    mean = sum/numOfGrades;
    
    return mean;
    
}

/*
 * standardDeviation(double grades[], int numOfGrades)
 * INPUT:  double array of grades & amount of grades
 * OUTPUT: standard deviation of grades
 */
double standardDeviation(double grades[], int numOfGrades) {
    
    double stddev;
    double meanValue = mean(grades, numOfGrades);
    double sum = 0;
    int i;
    
    for (i = 0; i < numOfGrades; i++) {
        sum = sum + ((meanValue - grades[i])*(meanValue - grades[i]));
    }
    
    stddev = sqrt(sum/numOfGrades);
    
    return stddev;
}
