import UIKit
import SpriteKit

extension SKNode {
    class func unarchiveFromFile(file : NSString) -> SKNode? {
        
        let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks")
        
        var sceneData = NSData.dataWithContentsOfFile(path, options: .DataReadingMappedIfSafe, error: nil)
        var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
        
        archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
        let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as GameScene
        archiver.finishDecoding()
        return scene
    }
}

class GameViewController: UIViewController {
    
    struct CardOutlets {
        var cardNameLabel: UILabel
    }
    
    // ==================== GLOBAL VARS ==================== //
    @IBOutlet var myTurnLabel: UILabel
    @IBOutlet var computerTurnLabel: UILabel
    @IBOutlet var pManaCountLabel: UILabel
    @IBOutlet var cManaCountLabel: UILabel
    @IBOutlet var pDeckCountLabel: UILabel
    @IBOutlet var cDeckCountLabel: UILabel
    
    @IBOutlet var ph1CardButton: UIButton
    @IBOutlet var ph2CardButton: UIButton
    
    @IBOutlet var ph1CostLabel: UILabel
    @IBOutlet var ph1NameLabel: UILabel
    @IBOutlet var ph1AttackLabel: UILabel
    @IBOutlet var ph1DefenseLabel: UILabel
    @IBOutlet var ph2CostLabel: UILabel
    @IBOutlet var ph2NameLabel: UILabel
    @IBOutlet var ph2AttackLabel: UILabel
    @IBOutlet var ph2DefenseLabel: UILabel
    
    
    let player = Player()
    let computer = Player()

    var pHandSlots = Array<Array<UILabel>>()
    
    let wisp = Card(name: "Wisp", attack: 1, defense: 1, taunt: false, cost: 0)
    let murlocRaider = Card(name: "Murloc Raider", attack: 2, defense: 1, taunt: false, cost: 1)
    let goldshireFootman = Card(name: "Goldshire Footman", attack: 1, defense: 2, taunt: true, cost: 1)
    let bloodfenRaptor = Card(name: "Bloodfen Raptor", attack: 3, defense: 2, taunt: false, cost: 2)
    let riverCrocolisk = Card(name: "River Crocolisk", attack: 2, defense: 3, taunt: false, cost: 2)
    let frostwolfGrunt = Card(name: "Frostwolf Grunt", attack: 2, defense: 2, taunt: true, cost: 2)
    let magmaRager = Card(name: "Magma Rager", attack: 5, defense: 1, taunt: false, cost: 3)
    let ironfurGrizzly = Card(name: "Ironfur Grizzly", attack: 3, defense: 3, taunt: true, cost: 3)
    let chillwindYeti = Card(name: "Chillwind Yeti", attack: 4, defense: 5, taunt: false, cost: 4)
    let mogushunWarden = Card(name: "Mogushun Warden", attack: 1, defense: 7, taunt: true, cost: 4)
    // ========================= END ========================== //
    
    
    // ==================== TIMER HANDLING ==================== //
    @IBOutlet var timerLabel: UILabel
    var timerVal = 10
    var timer: NSTimer!
    
    func startTimer() {
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateTimer:"), userInfo: nil, repeats: true)
    }
    
    func updateTimer(dt: NSTimer) {
        timerVal--
        if timerVal == 0 {
            if player.turn == true {
                computerTurn()
            } else {
                playerTurn()
            }
            timerLabel.textColor = UIColor.blackColor()
            timerVal = 11
        } else {
            timerLabel.text = "\(timerVal)"
        }
        
        if timerVal <= 10 {
            timerLabel.textColor = UIColor.redColor()
        }
    }
    // ========================= END ========================== //

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let scene = GameScene.unarchiveFromFile("GameScene") as? GameScene {
            // Configure the view.
            let skView = self.view as SKView
            skView.showsFPS = true
            //skView.showsNodeCount = true
            
            skView.ignoresSiblingOrder = true
            scene.scaleMode = .AspectFill
            skView.presentScene(scene)
    
        }
        
        buildDeck()
        fillHandSlots()
        startTimer()
        determineTurn()
        

    }
    
    func buildDeck() {
        player.deck += [wisp, murlocRaider, goldshireFootman, bloodfenRaptor, riverCrocolisk, frostwolfGrunt, magmaRager, ironfurGrizzly, chillwindYeti, mogushunWarden]
        
        computer.deck += [wisp, murlocRaider, goldshireFootman, bloodfenRaptor, riverCrocolisk, frostwolfGrunt, magmaRager, ironfurGrizzly, chillwindYeti, mogushunWarden]
    }
    
    func fillHandSlots() {
        pHandSlots = [[ph1CostLabel, ph1NameLabel, ph1AttackLabel, ph1DefenseLabel], [ph2CostLabel, ph2NameLabel, ph2AttackLabel, ph2DefenseLabel]]
    }
    
    func determineTurn() {
        var onePlayerZeroComputer  = Int(arc4random_uniform(2))
        if onePlayerZeroComputer == 1 {
            player.turn = true
            computer.turn = false
        } else {
            computer.turn = true
            player.turn = false
        }
        
        if player.turn == true {
            playerTurn()
        } else {
            computerTurn()
        }
    }
    
    func playerTurn() {
        computerTurnLabel.hidden = true
        myTurnLabel.hidden = false
        player.turn = true
        computer.turn = false
        
        player.deckCount -= 1
        pDeckCountLabel.text = String(player.deckCount)
        
        if player.mana != 10 {
            player.mana += 1
            pManaCountLabel.text = String(player.mana)
        }
        
        drawCard(player, handLabels: pHandSlots)
    }
    func computerTurn() {
        myTurnLabel.hidden = true
        computerTurnLabel.hidden = false
        computer.turn = true
        player.turn = false
        
        computer.deckCount -= 1
        cDeckCountLabel.text = String(computer.deckCount)
        
        if computer.mana != 10 {
            computer.mana += 1
            cManaCountLabel.text = String(player.mana)
        }
        
        //drawCard(computer, cHandSlots)
    }
    
    func drawCard(player: Player, handLabels: Array<Array<UILabel>>) {
        var randomCard = Int(arc4random_uniform(10) + 1)
        var chosenCard: Card = player.deck[randomCard]
        
        // Check available board slot //
        for setOfLabels in handLabels {
            for individualLabel in setOfLabels {
                if individualLabel.hidden == true {
                    var tagNumber = individualLabel.tag
                    println("tagNumber: \(tagNumber)")
                    switch tagNumber {
                    case 1:
                        ph1CardButton.hidden = false
                        ph1CostLabel.hidden = false
                        ph1NameLabel.hidden = false
                        ph1AttackLabel.hidden = false
                        ph1DefenseLabel.hidden = false
                        ph1CostLabel.text = String(chosenCard.cost)
                        ph1NameLabel.text = chosenCard.name
                        ph1AttackLabel.text = String(chosenCard.attack)
                        ph1DefenseLabel.text = String(chosenCard.defense)
                    case 2:
                        ph1CardButton.hidden = false
                        ph2CostLabel.hidden = false
                        ph2NameLabel.hidden = false
                        ph2AttackLabel.hidden = false
                        ph2DefenseLabel.hidden = false
                        ph2CostLabel.text = String(chosenCard.cost)
                        ph2NameLabel.text = chosenCard.name
                        ph2AttackLabel.text = String(chosenCard.attack)
                        ph2DefenseLabel.text = String(chosenCard.defense)
                    default:
                        println("tagNumber not within 1-10 - drawCard()")
                    }
                    break
                }
                break
            }
            break
        }
        
        
    }

    
    
    // ==================== SCENE/MEMORY HANDLING ==================== //
    override func supportedInterfaceOrientations() -> Int {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return Int(UIInterfaceOrientationMask.AllButUpsideDown.toRaw())
        } else {
            return Int(UIInterfaceOrientationMask.All.toRaw())
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
}
