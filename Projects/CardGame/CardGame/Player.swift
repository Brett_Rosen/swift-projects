class Player {
    
    var health = 30
    var mana = 0
    var turn = false
    
    var deckCount = 30
    var handCount = 10
    var deck = [Card]()
    var hand = [Card]()
    
    func endTurn() {
        self.mana += 1
    }
    func playCard(creature: Card, var field: [Card]) {
        if self.mana >= creature.cost {
            field.append(creature)
            self.mana -= creature.cost
        } else {
            "You do not have enough mana to play that card!"
        }
    }
}
