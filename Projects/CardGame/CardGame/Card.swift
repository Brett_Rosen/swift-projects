class Card {
    var name: String
    var attack: Int
    var defense: Int
    var taunt: Bool
    var cost: Int
    
    init(name: String, attack: Int, defense: Int, taunt: Bool, cost: Int) {
        self.name = name
        self.attack = attack
        self.defense = defense
        self.taunt = taunt
        self.cost = cost
    }
    
    func attackPlayer(playerHealth: Player, creaturesInPlay: [Card]) {
        for card in creaturesInPlay {
            if card.taunt {
                "You cannot attack the player!"
            } else {
                playerHealth.health -= self.attack
                if playerHealth.health <= 0 {
                    "Player has been killed!"
                }
            }
        }
    }
}